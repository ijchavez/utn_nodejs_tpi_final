const mongoose = require("../config/mongodb")
const bcrypt = require("bcrypt");

const usersSchema = mongoose.Schema({
    name:{
        type:String,
        required:[true, "El campo {PATH} es obligatorio"],
        minLength:3,

    },
    email:{
        type:String,
        required:[true, "El campo {PATH} es obligatorio"],
        minLength:5,

    },
    password:{
        type:String,
        required:[true, "El campo {PATH} es obligatorio"],
        minLength: 8,

    }
    
})
usersSchema.pre("save", function(next){
    this.password = bcrypt.hashSync(this.password, 10);
    next();
})
usersSchema.set("toJSON",{getters:true,setters:true})
module.exports = mongoose.model("users",usersSchema)