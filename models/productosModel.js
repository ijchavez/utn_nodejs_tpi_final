const mongoose = require("../config/mongodb")

const productosSchema = mongoose.Schema({
    name:{
        type:String,
        required:[true, "El campo {PATH} es obligatorio"],
        minLength:3,

    },
    description:{
        type:String,
        required:[true, "El campo {PATH} es obligatorio"],
        minLength:3,
        maxLength:50
    },
    price:{
        type:Number,
        required:[true, "El campo {PATH} es obligatorio"],
        min:0
    },
    code:{
        type:String,
        required:[true, "El campo {PATH} es obligatorio"],
        minLength:10,
        maxLength:10
    },
    category:{
        type:String,
        required:[true, "El campo {PATH} es obligatorio"],
        minLength:3,
        maxLength:50
    },
    image:String,
    highlighted:{
        type: Boolean,
        default:false,
        required: true
    }
    
})
productosSchema.set("toJSON",{getters:true,setters:true})
module.exports = mongoose.model("productos",productosSchema)