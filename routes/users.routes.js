var express = require('express');
var router = express.Router();

const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

const UsersService = require('../services/users.service.js')
const service = new UsersService();

/* GET users listing. */
router.post('/', async (req, res, next) => {
  try{
      const body = req.body;
      const newUser = await service.create(body);
      res.status(201).json(newUser);

  }catch(error){
      //res.status(400).json(error);
      error.status = 400;
      next(error);

  }

})
router.post('/login', async (req, res, next) => {
  try{
    const user = await service.findOne(req.body.email);
    if(!user){
      res.json({
        error:true,
        message:"invalid user"

      })
      return;
    }
    if(bcrypt.compareSync(req.body.password, user.password)){
      const token = jwt.sign(
                    {userId:user._id},
                    req.app.get("secretKey"),
                    {expiresIn:"1h"});
      res.json({token});

    }else{
      res.json({
        error:true,
        message:"invalid password"

      })
    }
  }catch(error){
    //res.status(400).json(error);
    error.status = 400;
    next(error);

  }

})

module.exports = router;
