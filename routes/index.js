var express = require('express');
var router = express.Router();

require('dotenv').config();
const jwt = require("jsonwebtoken");

const productsRouter = require('./products.routes');
const usersRouter = require('./users.routes');

const api = '/api';
const v1 = '/v1';

function baseRouter(baseEndpoint, app){
  const router = express.Router();
  app.use(baseEndpoint, router);

  return router;

}

function routerApi(app){
  const router = baseRouter(api + v1, app);

  router.use('/products', productsRouter);
  router.use('/users', usersRouter);

}

module.exports = routerApi;
