const express = require('express');
const router = express.Router();

const ProductsService = require('../services/product.service.js')
const service = new ProductsService();

router.get('/', async (req, res) => {
    const products = await service.find();
    res.json(products);
  
});
router.get('/highlighted', async (req, res) => {
  const products = await service.findHighlighted();
  res.json(products);

});
router.get('/:id',
  async (req, res, next) =>{
    try{
      const { id }= req.params;
      const product = await service.findOne(id);

      res.json(product);
      
    }catch(error){
      //res.status(404).json(error);
      error.status = 404;
      next(error);

    }

  }

);

router.post('/',(req, res, next) => req.app.verifyToken(req, res, next),
    async (req, res, next) => {
    try{
      const body = req.body;
      const newProduct = await service.create(body);
      res.status(201).json(newProduct);

    }catch(error){
       //res.status(400).json(error);
       error.status = 400;
       next(error);

    }


  }

)

router.patch('/:id',
    async (req, res, next) => {
      try{
        const { id } = req.params;
        const body = req.body;
        
        const product = await service.update(id, body)
        res.json(product);

      }catch(error){
        //res.json(error);
        error.status = 404;
        next(error);

      }

    }

)
router.delete('/:id', 
    async (req, res) => {
      try{
        const { id } = req.params;
        const product = await service.delete(id);
    
        res.status(204).json(product);
      }catch(error){
        //res.json(error);
        error.status = 404;
        next(error);

      }  


})
module.exports = router;