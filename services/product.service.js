const faker = require('faker');
const boom = require('@hapi/boom');
const productosModel = require("../models/productosModel")

class ProductsService {
  async find(){
    return await productosModel.find();

  }
  async findHighlighted(){
    const productHighlighted = await productosModel.find({highlighted:true});
    return productHighlighted;

  }
  async findOne(id){
    const product = await productosModel.findById(id)
    if(!product){
     throw boom.notFound('product not found');

    }
    return product;

 }
 async create(data){
  const newProduct = new productosModel({
    ...data

  })
  const document = await newProduct.save();
  return document;

}
 async update(id, changes){
   const product = await productosModel.updateOne({_id : id}, changes)
   return product

 }
 async delete(id){
   const product = await productosModel.deleteOne({_id : id})
   return product

 }
  
}

module.exports = ProductsService
