const usersModel = require("../models/usersModel")

class UsersService {
  async findOne(email){
    const user = await usersModel.findOne({ email:email });
    return user;

  }
 async create(data){
  const newUser = new usersModel({
    ...data

  })
  const document = await newUser.save();
  return document;

 }
  
}

module.exports = UsersService
